package com.github.webuitesting.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.seleniumhq.selenium.fluent.FluentWebDriver;
import org.testng.Assert;

/*
 * Sample page

 */
public class HomePage extends BasePage<HomePage> {

	private final String HEADER = "lst-ib";
	private final String URL = "/";

	@FindBy(how = How.ID, using = HEADER)
	@CacheLookup
	private WebElement headerElement;

	public HomePage(WebDriver webDriver, String baseUrl) {
		super(webDriver,baseUrl);
		PageFactory.initElements(webDriver, this);
	}

	public String getHeaderText() {
		return headerElement.getText();
	}

	@Override
	protected void load() {
		webDriver.get( baseUrl + URL);
	}

	@Override
	protected void isLoaded() throws Error {
		Assert.assertTrue(  webDriver.getTitle().contains("Google")) ;
	}
}
