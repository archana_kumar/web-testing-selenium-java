package com.github.webuitesting.homepage;

import com.github.webuitesting.TestBase;
import com.github.webuitesting.pages.HomePage;
import com.relevantcodes.extentreports.ExtentTest;
import org.testng.annotations.Test;

import static org.assertj.core.api.Assertions.*;

public class HomePageTest extends TestBase {

	@Test
	public void testDivExisting() throws InterruptedException {
        ExtentTest extentTest = extent.startTest("Test Div Existing");
        HomePage homepage = new HomePage( webDriver, websiteUrl).get();
		String headerText = homepage.getHeaderText();
		assertThat(headerText != null);
        extent.endTest(extentTest);
	}

}
